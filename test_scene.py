#!/usr/bin/env python3
from __future__ import annotations

import math
import random

import pygame
from cvectors import Vector

from phys import Scene, materials, objects
from phys.render import Drawer

DT = 1 / 240
FPS = 60
AA = 1


def get_shape(position: Vector) -> objects.Object:
    material = random.choice(materials.materials)
    if random.randrange(3):
        return objects.Circle(
            position=position, material=material, radius=random.uniform(0.03, 0.15)
        )
    else:
        points = random.choice(
            (
                (
                    Vector(0.2, 0.02),
                    Vector(-0.2, 0.02),
                    Vector(-0.2, -0.02),
                    Vector(0.2, -0.02),
                ),
                (
                    Vector.from_polar(0.1, 0),
                    Vector.from_polar(0.1, math.tau / 3),
                    Vector.from_polar(0.1, 2 * math.tau / 3),
                ),
            )
        )
        return objects.ConvexSolid(position=position, material=material, points=points)


def create_drawer(screen_size: Vector) -> Drawer:
    return Drawer(
        pygame.Surface((screen_size * AA).round()).convert(), 0.5 * screen_size.x * AA
    )


def simulate(scene: Scene) -> None:
    screen_size = Vector(1600, 1600)
    S = pygame.display.set_mode(screen_size.round(), pygame.RESIZABLE)
    pygame.display.set_caption("Phys")
    drawer = create_drawer(screen_size)
    clock = pygame.time.Clock()
    real_time: float = 0
    time_: float = 0
    while True:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                scene.objects.append(
                    get_shape(drawer.untransform(Vector(*event.pos) * AA))
                )
            elif event.type == pygame.VIDEORESIZE:
                screen_size = Vector(*event.size)
                drawer = create_drawer(screen_size)
            elif event.type == pygame.QUIT:
                return

        d_cols = []
        while time_ <= real_time:
            d_cols += scene.tick(DT)
            time_ += DT
        drawer.draw(scene, d_cols)
        S.blit(
            pygame.transform.smoothscale(drawer.surface, screen_size.round()), (0, 0)
        )
        pygame.display.flip()
        real_time += 1 / FPS
        clock.tick(FPS)


def main() -> None:
    scene = Scene(
        [
            objects.HalfPlane(
                position=Vector(0, 0),
                material=materials.glass,
                normal=Vector(0, 1),
                affected_by_gravity=False,
            ),
            objects.HalfPlane(
                position=Vector(0, 0),
                material=materials.glass,
                normal=Vector(1, 0),
                affected_by_gravity=False,
            ),
            objects.HalfPlane(
                position=Vector(2, 2),
                material=materials.glass,
                normal=Vector(-1, 0),
                affected_by_gravity=False,
            ),
        ]
    )
    simulate(scene)


if __name__ == "__main__":
    main()
