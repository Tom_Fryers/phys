from __future__ import annotations

import itertools
import math
from dataclasses import dataclass

from cvectors import Vector

from phys import materials
from phys.objects import Object


def can_intersect(
    bbox_0: tuple[Vector, Vector], bbox_1: tuple[Vector, Vector], /
) -> bool:
    return (
        bbox_1[0].x <= bbox_0[1].x
        and bbox_0[0].x <= bbox_1[1].x
        and bbox_1[0].y <= bbox_0[1].y
        and bbox_0[0].y <= bbox_1[1].y
    )


def resolve_collision(
    pair: tuple[Object, Object], normal: Vector, point: Vector
) -> None:
    try:
        unit_normal = normal.hat()
    except ZeroDivisionError:
        return
    e = (
        pair[0].material.coefficient_of_restitution
        + pair[1].material.coefficient_of_restitution
    ) / 2
    angle = unit_normal.theta
    u_0 = pair[0].velocity.rotate(-angle)
    u_1 = pair[1].velocity.rotate(-angle)
    m_0 = pair[0].mass
    m_1 = pair[1].mass
    i_0 = pair[0].moment_of_inertia
    i_1 = pair[1].moment_of_inertia
    r_0 = (point - pair[0].position).rotate(-angle)
    r_1 = (point - pair[1].position).rotate(-angle)

    relative_velocity = (
        u_1
        + pair[1].angular_velocity * r_1.perp()
        - (u_0 + pair[0].angular_velocity * r_0.perp())
    )
    impulse = (
        -(1 + e)
        * relative_velocity.x
        / (1 / m_0 + 1 / m_1 + r_0.y**2 / i_0 + r_1.y**2 / i_1)
    )
    static, dynamic = materials.friction(pair[0].material, pair[1].material)
    static *= abs(impulse)
    dynamic *= abs(impulse)

    necessary_impulse = (
        impulse * (r_1.y * r_1.x / i_1 - r_0.y * r_0.x / i_0) - relative_velocity.y
    ) / (1 / m_1 + 1 / m_0 + r_0.x**2 / i_0 + r_1.x**2 / i_1)
    if abs(necessary_impulse) < static:
        friction_impulse = necessary_impulse
    else:
        friction_impulse = math.copysign(dynamic, necessary_impulse)

    impulse_v = Vector(impulse, friction_impulse)
    new_angular_velocity = (
        pair[0].angular_velocity + impulse_v.perp_dot(r_0) / i_0,
        pair[1].angular_velocity - impulse_v.perp_dot(r_1) / i_1,
    )

    pair[0].velocity = (u_0 - impulse_v / m_0).rotate(angle)
    pair[1].velocity = (u_1 + impulse_v / m_1).rotate(angle)
    pair[0].angular_velocity = new_angular_velocity[0]
    pair[1].angular_velocity = new_angular_velocity[1]
    if abs(impulse) > 0:
        total_inverse_mass = 1 / m_0 + 1 / m_1
        pair[0].position -= normal / m_0 / total_inverse_mass
        pair[1].position += normal / m_1 / total_inverse_mass


@dataclass(slots=True)
class Scene:
    objects: list[Object]
    gravity: Vector = Vector(0, -9.80665)

    def tick(self, dt: float) -> list[tuple[tuple[Object, Object], Vector, Vector]]:
        for object_ in self.objects:
            if object_.affected_by_gravity:
                object_.velocity += self.gravity * dt
            object_.move(dt)

        collisions: list[tuple[tuple[Object, Object], Vector, Vector]] = []
        for (bbox_0, object_0), (bbox_1, object_1) in itertools.combinations(
            tuple((s.bounding_box, s) for s in self.objects), 2
        ):
            if can_intersect(bbox_0, bbox_1):
                collisions.extend(
                    ((object_0, object_1), *c) for c in object_0.collides(object_1)
                )
        for collision in collisions:
            resolve_collision(*collision)
        return collisions
