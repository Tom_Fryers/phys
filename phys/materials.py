from dataclasses import dataclass


@dataclass(frozen=True, slots=True)
class Material:
    density: float
    coefficient_of_restitution: float
    colour: tuple[int, int, int]


steel = Material(7900, 0.56, (154, 163, 163))
iron = Material(7874, 0.45, (161, 157, 148))
copper = Material(8960, 0.15, (184, 115, 51))
aluminium = Material(2700, 0.1, (214, 214, 214))
lead = Material(11340, 0.08, (67, 78, 82))
wood = Material(500, 0.5, (215, 170, 96))
ice = Material(917, 0.88, (113, 166, 210))
rubber = Material(1100, 0.9, (133, 104, 77))
glass = Material(2520, 0.94, (240, 240, 240))

metals = {steel, iron, copper, aluminium, lead}
materials = (*metals, wood, ice, rubber, glass)


def friction(m: Material, n: Material, /) -> tuple[float, float]:
    if m in metals:
        m = steel
    if n in metals:
        n = steel
    if m == steel:
        if n == steel:
            return (0.65, 0.42)
        elif n == wood:
            return (0.5, 0.3)
        elif n == ice:
            return (0.03, 0.02)
        elif n == rubber:
            return (1.0, 0.8)
        elif n == glass:
            return (0.68, 0.53)
    elif m == wood:
        if n == wood:
            return (0.58, 0.4)
        elif n == ice:
            return (0.05, 0.02)
        elif n == rubber:
            return (0.9, 0.65)
        elif n == glass:
            return (0.4, 0.3)
    elif m == ice:
        if n == ice:
            return (0.1, 0.03)
        elif n == rubber:
            return (0.1, 0.05)
        elif n == glass:
            return (0.02, 0.02)
    elif m == rubber:
        if n == rubber:
            return (1.16, 0.8)
        elif n == glass:
            return (0.9, 0.65)
    elif m == glass:
        if n == glass:
            return (0.94, 0.4)
    return friction(n, m)
