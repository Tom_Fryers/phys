from __future__ import annotations

import bisect
import math
from abc import ABC, abstractmethod
from collections.abc import Iterator
from dataclasses import dataclass, field

from cvectors import Vector

from phys.materials import Material


@dataclass(slots=True, kw_only=True)
class Object(ABC):
    position: Vector
    velocity: Vector = Vector(0, 0)
    angle: float = 0
    angular_velocity: float = 0
    material: Material
    affected_by_gravity: bool = True

    @property
    @abstractmethod
    def area(self) -> float:
        ...

    @property
    def mass(self) -> float:
        return self.area * self.material.density

    @property
    @abstractmethod
    def moment_of_inertia(self) -> float:
        ...

    def move(self, dt: float) -> None:
        self.velocity *= 0.95**dt
        self.angular_velocity *= 0.95**dt
        self.position += self.velocity * dt
        self.angle += self.angular_velocity * dt

    @abstractmethod
    def collides(self, other: Object) -> Iterator[tuple[Vector, Vector]]:
        ...

    @property
    def bounding_box(self) -> tuple[Vector, Vector]:
        return (Vector(-math.inf, -math.inf), Vector(math.inf, math.inf))


@dataclass(slots=True, kw_only=True)
class HalfPlane(Object):
    normal: Vector

    def collides(self, other: Object) -> Iterator[tuple[Vector, Vector]]:
        if isinstance(other, HalfPlane):
            return
        if isinstance(other, Circle):
            centre_distance = (other.position - self.position).dot(self.normal)
            overlap = other.radius - centre_distance
            if overlap > 0:
                yield (
                    self.normal * overlap,
                    other.position - self.normal * other.radius,
                )
            return
        if isinstance(other, ConvexSolid):
            for point in other.absolute_points:
                overlap = (self.position - point).dot(self.normal)
                if overlap > 0:
                    yield (self.normal * overlap, point)
            return

    @property
    def area(self) -> float:
        return math.inf

    @property
    def moment_of_inertia(self) -> float:
        return math.inf

    @property
    def bounding_box(self) -> tuple[Vector, Vector]:
        if self.normal.x == 0:
            if self.normal.y > 0:
                return (Vector(-math.inf, -math.inf), Vector(math.inf, self.position.y))
            else:
                return (Vector(-math.inf, self.position.y), Vector(math.inf, math.inf))
        elif self.normal.y == 0:
            if self.normal.x > 0:
                return (Vector(-math.inf, -math.inf), Vector(self.position.x, math.inf))
            else:
                return (Vector(self.position.x, -math.inf), Vector(math.inf, math.inf))
        return super().bounding_box


@dataclass(slots=True, kw_only=True)
class Circle(Object):
    radius: float

    def collides(self, other: Object) -> Iterator[tuple[Vector, Vector]]:
        if not isinstance(other, Circle):
            yield from ((-o, p) for o, p in other.collides(self))
            return
        diff = other.position - self.position
        centre_distance = abs(diff)
        overlap = self.radius + other.radius - centre_distance
        if overlap > 0:
            diff /= centre_distance
            yield (diff * overlap, self.position + diff * self.radius)

    @property
    def area(self) -> float:
        return math.pi * self.radius**2

    @property
    def moment_of_inertia(self) -> float:
        return self.material.density * math.pi * self.radius**4 / 2

    @property
    def bounding_box(self) -> tuple[Vector, Vector]:
        return (
            self.position - Vector(self.radius, self.radius),
            self.position + Vector(self.radius, self.radius),
        )


@dataclass(slots=True, kw_only=True)
class ConvexSolid(Object):
    points: tuple[Vector, ...]
    absolute_points: list[Vector] = field(init=False)
    segments: list[tuple[Vector, Vector, Vector, Vector]] = field(init=False)

    def solid_has_vertex_in(
        self, other: ConvexSolid
    ) -> Iterator[tuple[Vector, Vector]]:
        for point in other.absolute_points:
            index = bisect.bisect(
                [p.theta % math.tau for p in self.points],
                ((point - self.position).theta - self.angle) % math.tau,
            )
            # Segment normal points inwards, as the points are anticlockwise
            segment_vector, segment_normal, p0, p1 = self.segments[index - 1]
            overlap = (point - p0).dot(segment_normal)
            if overlap >= 0 and p0.dot(segment_vector) <= point.dot(
                segment_vector
            ) <= p1.dot(segment_vector):
                yield (-segment_normal * overlap, point)

    def collides(self, other: Object) -> Iterator[tuple[Vector, Vector]]:
        if isinstance(other, Circle):
            for point in self.absolute_points:
                diff = other.position - point
                distance = abs(diff)
                overlap = other.radius - distance
                if overlap > 0:
                    yield (diff * overlap / distance, point)
            for segment_vector, segment_normal, p0, p1 in self.segments:
                # Points inwards, as the points are anticlockwise
                closest_point = other.position + other.radius * segment_normal
                if (closest_point - self.position).dot(segment_normal) >= 0:
                    continue
                overlap = (closest_point - p0).dot(segment_normal)
                if overlap >= 0 and p0.dot(segment_vector) <= closest_point.dot(
                    segment_vector
                ) <= p1.dot(segment_vector):
                    yield (-segment_normal * overlap, closest_point)
            return
        if isinstance(other, ConvexSolid):
            yield from self.solid_has_vertex_in(other)
            yield from ((-o, p) for o, p in other.solid_has_vertex_in(self))
            return
        yield from ((-o, p) for o, p in other.collides(self))

    @property
    def area(self) -> float:
        return sum(p0.perp_dot(p1) for _, _, p0, p1 in self.segments) / 2

    @property
    def moment_of_inertia(self) -> float:
        approx_radius = sum(abs(p) for p in self.points) / len(self.points)
        return self.material.density * math.pi * approx_radius**4 / 2

    def move(self, dt: float) -> None:
        Object.move(self, dt)
        self.absolute_points = [
            self.position + point.rotate(self.angle) for point in self.points
        ]
        self.segments = [
            (v := (v1 - v0).hat(), v.perp(), v0, v1)
            for v0, v1 in zip(
                self.absolute_points,
                self.absolute_points[1:] + [self.absolute_points[0]],
            )
        ]

    @property
    def bounding_box(self) -> tuple[Vector, Vector]:
        return (
            Vector(
                min(p.x for p in self.absolute_points),
                min(p.y for p in self.absolute_points),
            ),
            Vector(
                max(p.x for p in self.absolute_points),
                max(p.y for p in self.absolute_points),
            ),
        )
