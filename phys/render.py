from __future__ import annotations

from dataclasses import dataclass

import pygame
from cvectors import Vector

from phys import Scene, objects
from phys.objects import Object


@dataclass
class Drawer:
    surface: pygame.surface.Surface
    scale: float

    @property
    def size(self) -> Vector:
        return Vector(*self.surface.get_size())

    def draw(
        self, scene: Scene, d_cols: list[tuple[tuple[Object, Object], Vector, Vector]]
    ) -> None:
        self.surface.fill((255, 255, 255))
        for object_ in scene.objects:
            if isinstance(object_, objects.Circle):
                pygame.draw.circle(
                    self.surface,
                    object_.material.colour,
                    self.transform(object_.position),
                    object_.radius * self.scale,
                )
                pygame.draw.line(
                    self.surface,
                    (180, 180, 180),
                    self.transform(object_.position),
                    self.transform(
                        object_.position
                        + Vector.from_polar(object_.radius, object_.angle)
                    ),
                    2,
                )
            elif isinstance(object_, objects.ConvexSolid):
                pygame.draw.polygon(
                    self.surface,
                    object_.material.colour,
                    [self.transform(p) for p in object_.absolute_points],
                )
        for _, normal, point in d_cols:
            pygame.draw.line(
                self.surface,
                (200, 30, 30),
                self.transform(point),
                self.transform(point + normal * 50),
                round(min(self.size) / 800),
            )

    def transform(self, pos: Vector, /) -> Vector:
        pos = pos * self.scale - self.size / 2
        pos = Vector(pos.x, -pos.y)
        return pos + self.size / 2

    def untransform(self, pos: Vector, /) -> Vector:
        pos = pos - self.size / 2
        pos = Vector(pos.x, -pos.y)
        return (pos + self.size / 2) / self.scale
